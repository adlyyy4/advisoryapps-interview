class ProfilePictureModel {
  final String? url;
  final int? width;
  final int? height;

  const ProfilePictureModel({this.url, this.width, this.height});

  factory ProfilePictureModel.fromJson(Map<String, dynamic> json) =>
      ProfilePictureModel(
          url: json['url'], width: json['width'], height: json['height']);
}
