import 'package:advisoryapps_inteview/Models/ProfilePictureModel.dart';

class FacebookUserModel {
  final String? email;
  final String? name;
  final ProfilePictureModel? pictureModel;

  const FacebookUserModel({this.email, this.name, this.pictureModel});
  factory FacebookUserModel.fromJson(Map<String, dynamic> json) =>
      FacebookUserModel(
          email: json['email'],
          name: json['name'],
          pictureModel: ProfilePictureModel.fromJson(json['picture']['data']));
}
