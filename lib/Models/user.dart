class User {
  final String id, email, password, token;

  const User(
      {required this.email,
      required this.id,
      required this.password,
      required this.token});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        id: json['id'],
        email: json['email'],
        password: json['password'],
        token: json['token']);
  }

  Map<String, dynamic> toMap() {
    return {'email': email, 'password': password, 'token': token};
  }
}
