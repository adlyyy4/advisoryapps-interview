class Listing {
  String id, list_name, distance;

  Listing(this.id, this.list_name, this.distance);

  Listing.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        list_name = json['list_name'],
        distance = json['distance'];

  Map<String, dynamic> toJson() =>
      {'id': id, 'list_namee': list_name, 'distance': distance};
}
