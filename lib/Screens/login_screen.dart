import 'package:advisoryapps_inteview/Services/facebookServices.dart';
import 'package:advisoryapps_inteview/Services/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

//final _key = GlobalKey<FormState>();
TextEditingController emailController = TextEditingController();
TextEditingController passwordController = TextEditingController();

class _LoginPageState extends State<LoginPage> {
  final apiService = ApiService();

  @override
  Widget build(BuildContext context) {
    FacebookService facebookService = Provider.of<FacebookService>(context);
    void _onSignIn() async {
      Map? result =
          await apiService.login(emailController.text, passwordController.text);
      if (result != null && result['code'] == 200) {
        if (!mounted) return;
        Navigator.pushReplacementNamed(context, '/dashboard');
      } else {
        if (!mounted) return;
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(result!['message'])));
      }
    }

    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
          body: FutureBuilder<dynamic>(
              future: apiService.getTokenAndID(
                  () => Navigator.pushReplacementNamed(context, '/dashboard')),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(child: CircularProgressIndicator());
                }

                return Form(
                    child: ListView(
                  children: <Widget>[
                    const SizedBox(
                      height: 250,
                    ),
                    Container(
                      padding: const EdgeInsets.all(10),
                      child: TextFormField(
                        controller: emailController,
                        onChanged: (value) {},
                        decoration: InputDecoration(
                            hintText: 'Email',
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            labelText: 'Enter Email'),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                      child: TextFormField(
                        controller: passwordController,
                        onChanged: (value) {},
                        obscureText: true,
                        decoration: InputDecoration(
                          hintText: 'Password',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          labelText: 'Password',
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    Container(
                        height: 50,
                        padding: const EdgeInsets.fromLTRB(6, 0, 6, 0),
                        child: ElevatedButton(
                          onPressed: () {
                            _onSignIn();
                          },
                          style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0.0),
                          )),
                          child: const Text('Login'),
                        )),
                    const Divider(),
                    Container(
                      height: 50,
                      padding: const EdgeInsets.fromLTRB(6, 0, 6, 0),
                      child: SignInButton(
                        Buttons.Facebook,
                        text: "Sign in with Facebook",
                        onPressed: () async {
                          dynamic result = await facebookService.fbLogin();
                          if (result == true) {
                            if (!mounted) return;
                            Navigator.pushReplacementNamed(
                                context, '/facebookDashboard');
                          }
                        },
                      ),
                    ),
                  ],
                ));
              })),
    );
  }
}
