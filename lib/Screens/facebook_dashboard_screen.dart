import 'package:advisoryapps_inteview/Models/facebook_user_model.dart';
import 'package:advisoryapps_inteview/Services/facebookServices.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FacebookDashboard extends StatefulWidget {
  const FacebookDashboard({Key? key}) : super(key: key);

  @override
  State<FacebookDashboard> createState() => _FacebookDashboardState();
}

class _FacebookDashboardState extends State<FacebookDashboard> {
  @override
  Widget build(BuildContext context) {
    FacebookService facebookService = Provider.of<FacebookService>(context);
    FacebookUserModel user = facebookService.currentUser!;

    return GestureDetector(
      child: Scaffold(
          body: Container(
        padding: const EdgeInsets.only(top: 100),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Column(
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: CircleAvatar(
                      radius: user.pictureModel!.width! / 3,
                      backgroundImage: NetworkImage(user.pictureModel!.url!)),
                ),
                const Divider(),
                Text(
                  user.name!,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, height: 4, fontSize: 25),
                ),
                Text(
                  user.email!,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, height: 1, fontSize: 25),
                ),
                const SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                    onPressed: () {
                      facebookService.fbLogout();
                      Navigator.pushReplacementNamed(context, '/');
                    },
                    child: const Text('Log out'))
              ],
            ),
          ],
        ),
      )),
    );
  }
}
