import 'package:advisoryapps_inteview/Models/listing.dart';
import 'package:advisoryapps_inteview/Services/services.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    ApiService apiService = Provider.of<ApiService>(context);
    return GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.blueAccent[900],
            title: const SizedBox(
              width: 50,
            ),
            centerTitle: true,
            leading: const Icon(Icons.account_circle_rounded),
            actions: <Widget>[
              TextButton(
                  onPressed: () => apiService.logout(() {
                        Navigator.pushReplacementNamed(context, '/');
                      }),
                  child: const Icon(Icons.logout, color: Colors.white)),
            ],
          ),
          body: FutureBuilder<List>(
            future: apiService.getList(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                //print(snapshot.data);
                return const Center(child: CircularProgressIndicator());
              } else {
                return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    Listing itemList = Listing.fromJson(snapshot.data[index]);

                    return ListTile(
                      title: Text('Name: ${itemList.list_name}'),
                      subtitle: Text('Distance: ${itemList.distance}'),
                      trailing: const Icon(Icons.info_outline),
                      onTap: () {
                        Fluttertoast.showToast(
                            msg:
                                "Name:${itemList.list_name} Distance: ${itemList.distance}");
                      },
                    );
                  },
                );
              }
            },
          ),
        ));
  }
}
