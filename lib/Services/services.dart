// ignore_for_file: avoid_print
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ApiService {
  String api = 'https://interview.advisoryapps.com/index.php';

  //Login function with login API using email and password (post)
  Future<Map?> login(String email, String password) async {
    Map data = {'email': email, 'password': password};
    final res = await http.post(Uri.parse('$api/login'), body: data);
    final prefs = await SharedPreferences.getInstance();
    var json = jsonDecode(res.body);

    if (res.statusCode == 200) {
      //status code from the response api
      int? code = json['status']['code'];
      if (code == 200) {
        //saved token using shared preference
        await prefs.setString('token', json['token']);

        //saved id using shared preference
        await prefs.setString('id', json['id']);

        // print(json.toString());
      }
    }

    return json['status'];
  }

  //get user token to automatically sign in
  Future<dynamic> getTokenAndID(VoidCallback callback) async {
    final prefs = await SharedPreferences.getInstance();

    //get token using shared preferences
    String? token = prefs.getString('token');

    //get id using shared preferences
    String? id = prefs.getString('id');
    if (token != null) {
      final res = await http.get(Uri.parse('$api/listing?id=$id&token=$token'));

      if (res.statusCode == 200) {
        callback();
      }
    }
  }

  //log out
  Future<dynamic> logout(VoidCallback callback) async {
    final prefs = await SharedPreferences.getInstance();
    final id = await prefs.remove('id');
    final token = await prefs.remove('token');
    // print('token:$token, id:$id');
    callback();
  }

  // get list of listing
  Future<List> getList() async {
    final prefs = await SharedPreferences.getInstance();
    //get token to access data in listing
    String? token = prefs.getString('token');

    //get user id to access data in listing
    String? id = prefs.getString('id');

    final res = await http.get(Uri.parse('$api/listing?id=$id&token=$token'));
    final jsonData = jsonDecode(res.body);

    List listing = [];
    int? code = jsonData['status']['code'];
    String? message = jsonData['status']['message'];

    // print(code);
    // print('token:$token, id:$id');
    // print(message);
    if (res.statusCode == 200) {
      listing = jsonData['listing'];
      //print(jsonData['listing']);
    }

    return listing;
  }
}
