// ignore_for_file: unused_field, file_names

import 'package:advisoryapps_inteview/Models/facebook_user_model.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

class FacebookService {
  FacebookUserModel? _currentUser;
  AccessToken? _accessToken;
  Future<bool> fbLogin() async {
    final LoginResult fbresult = await FacebookAuth.i.login();

    if (fbresult.status == LoginStatus.success) {
      _accessToken = fbresult.accessToken;

      final data = await FacebookAuth.i.getUserData();
      FacebookUserModel model = FacebookUserModel.fromJson(data);
      _currentUser = model;
      //print(data);

      return true;
    } else {
      return false;
    }
  }

  FacebookUserModel? get currentUser => _currentUser;

  Future<void> fbLogout() async {
    await FacebookAuth.i.logOut();

    _currentUser = null;
    _accessToken = null;
    // print(_accessToken);
    // print(_currentUser);
  }
}
