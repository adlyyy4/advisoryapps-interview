import 'package:advisoryapps_inteview/Screens/dashboard_screen.dart';
import 'package:advisoryapps_inteview/Screens/facebook_dashboard_screen.dart';
import 'package:advisoryapps_inteview/Screens/login_screen.dart';
import 'package:advisoryapps_inteview/Services/facebookServices.dart';
import 'package:advisoryapps_inteview/Services/services.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'Screens/login_screen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<ApiService>(create: (_) => ApiService()),
        Provider<FacebookService>(create: (_) => FacebookService())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Advisory Apps Interview',
        theme: ThemeData(
          scaffoldBackgroundColor: Colors.white,
        ),
        // home: const LoginPage(),
        initialRoute: '/',
        routes: {
          '/': (context) => const LoginPage(),
          '/dashboard': (context) => const Dashboard(),
          '/facebookDashboard': (context) => const FacebookDashboard()
        },
      ),
    );
  }
}
